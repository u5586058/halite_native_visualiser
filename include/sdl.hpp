#ifndef SDL_H
#define SDL_H

#include "dimensions.hpp"
#include "halite_replay.hpp"
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <cstdlib>

#define MIN(a, b) (a < b) ? a : b

namespace sdl {
class Surface {
public:
  Surface(SDL_Surface *surface);
  ~Surface() { SDL_FreeSurface(surface); }
  SDL_Surface *surface;
};

class Window {
public:
  Window(SDL_Window *window);
  ~Window() { SDL_DestroyWindow(window); }
  SDL_Window *window;
};

class SDL {
public:
  typedef struct Color_t {
    uint8_t r;
    uint8_t g;
    uint8_t b;
  } Color;

  std::unique_ptr<Window> window;
  std::unique_ptr<Surface> screen_surface;
  Dimensions screen_dim;
  Dimensions board_display_dim;
  Dimensions board_dim;
  Dimensions tile_dim;
  int num_players;

  SDL(int num_players);
  ~SDL();

  void SetBoardDim(Dimensions board_dim);
  void SetTileDim(Dimensions tile_dim);
  uint32_t MapRGB(uint8_t r, uint8_t g, uint8_t b) const;
  uint32_t MapRGB(Color color, uint8_t intensity) const;
  uint32_t MapRGB(Color color) const;
  void FillRect(const SDL_Rect *rect, uint32_t color);
  void UpdateWindowSurface(void);
  void RenderFrame(const Frame &frame, const HaliteReplay &replay);
  void BlitSurface(SDL_Surface *src, SDL_Rect *srcrect, SDL_Rect *dstrect);

private:
  std::vector<Color> player_colors;
  // const Color black = {0x00, 0x00, 0x00};
  const Color white = {0xff, 0xff, 0xff};
  const Color red = {0xff, 0x00, 0x00};
  const Color green = {0x00, 0xff, 0x00};
  const Color blue = {0x00, 0x00, 0xff};
  const Color yellow = {0xff, 0xff, 0x00};
  const Color aqua = {0x00, 0xff, 0xff};
  const Color fuchsia = {0xff, 0x00, 0xff};
  std::vector<Color> player_color_choices;
};
}

#endif // SDL_H
