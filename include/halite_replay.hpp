#ifndef HALITEREPLAY_H
#define HALITEREPLAY_H

#include "dimensions.hpp"
#include "json11/json11.hpp"
#include <algorithm>
#include <iostream>

class HaliteReplay {
public:
  int num_players;
  int num_frames;
  Dimensions board_dimensions;
  std::vector<Frame> frames;
  Productions productions;

  HaliteReplay(const json11::Json &replay_json);
};

#endif // HALITEREPLAY_H
