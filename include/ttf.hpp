#ifndef TTF_H
#define TTF_H

#include <SDL2/SDL_ttf.h>
#include <algorithm>
#include <memory>

#include "dimensions.hpp"
#include "halite_game_info.hpp"
#include "sdl.hpp"

class TTF {
public:
  TTF(sdl::SDL &sdl);
  ~TTF();
  void RenderGameInfo(const Frame &frame, const Productions &productions);
  void DrawGameTitle(const std::vector<std::string> &player_names);

private:
  TTF_Font *font;
  sdl::SDL &sdl;
  const SDL_Color white = {0xff, 0xff, 0xff, 0xff};
  void CalculateGameInfo(HaliteGameInfo &game_info, const Frame &frame,
                         const Productions &productions) const;
  void MakeGameInfoStrings(std::vector<std::string> &game_info_strings,
                           const HaliteGameInfo &game_info) const;
  void DrawGameInfo(const std::vector<std::string> &game_info_strings);
  void ClearGameInfo();
  int num_players;
};

#endif // TTF_H
