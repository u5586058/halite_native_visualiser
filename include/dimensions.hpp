#ifndef DIMENSIONS_H
#define DIMENSIONS_H

#include "json11/json11.hpp"

typedef struct Dimensions_t {
  int w;
  int h;
} Dimensions;

typedef struct Site_t {
  uint8_t owner;
  uint8_t strength;

  Site_t(json11::Json site_json);
} Site;

using Frame = std::vector<std::vector<Site>>;
using Productions = std::vector<std::vector<uint8_t>>;

#endif // DIMENSIONS_H
