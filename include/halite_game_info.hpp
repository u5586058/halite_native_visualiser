#ifndef HALITEGAMEINFO_H
#define HALITEGAMEINFO_H

#include <vector>

class HaliteGameInfo {
public:
  std::vector<int> territory_data;
  std::vector<int> production_data;
  std::vector<int> strength_data;

  HaliteGameInfo(int num_players);
};

#endif // HALITEGAMEINFO_H
