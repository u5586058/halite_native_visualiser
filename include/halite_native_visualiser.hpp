#ifndef HALITE_NATIVE_VISUALISER_H
#define HALITE_NATIVE_VISUALISER_H

#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <vector>

#include "halite_replay.hpp"
#include "json11/json11.hpp"
#include "sdl.hpp"
#include "ttf.hpp"

namespace {
void GetJsonFromFile(json11::Json &replay,
                     const std::__cxx11::string &file_name);
std::vector<std::string> GetPlayerNames(const json11::Json &player_names_json);
}

#endif
