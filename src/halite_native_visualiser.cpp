// George Rayns <u5586058@anu.edu.au>
// Will be better documented in the future, I promise

#include "halite_native_visualiser.hpp"

namespace {
void GetJsonFromFile(json11::Json &replay, const std::string &file_name) {
  std::string json_string;
  std::ifstream replay_file(file_name);
  if (replay_file.is_open()) {
    std::getline(replay_file, json_string);
    replay_file.close();
  } else {
    throw std::runtime_error("Couldn't open json file!");
  }

  std::string err_string; // we don't really care about this
  replay = json11::Json::parse(json_string, err_string);
}

std::vector<std::string> GetPlayerNames(const json11::Json &player_names_json) {
  if (player_names_json.is_null()) {
    throw std::runtime_error("Couldn't get player names from json!");
  }
  std::vector<std::string> player_names;
  for (const auto &player_name_json : player_names_json.array_items()) {
    // TODO: Make a wrapper for string_value()
    player_names.push_back(player_name_json.string_value());
  }

  return player_names;
}
}

char default_file_name[] = "replay.hlt";

int main(int argc, char *argv[]) {
  char *file_name = default_file_name;
  int total_time = 5; // seconds

  // get some arguments
  // TODO: Do this properly
  if (argc > 1) {
    file_name = argv[1];
  }
  if (argc > 2) {
    total_time = std::stoi(argv[2]);
  }

  json11::Json root;
  GetJsonFromFile(root, file_name);

  HaliteReplay replay(root);

  sdl::SDL sdl(replay.num_players);

  TTF ttf(sdl);

  // can this be reworked?
  sdl.SetBoardDim(replay.board_dimensions);

  const int delay_ms = total_time * 1000 / replay.num_frames;

  sdl.SetTileDim({sdl.board_display_dim.w / sdl.board_dim.w,
                  sdl.board_display_dim.h / sdl.board_dim.h});

  ttf.DrawGameTitle(GetPlayerNames(root["player_names"]));

  for (const auto &frame : replay.frames) {
    sdl.RenderFrame(frame, replay);

    ttf.RenderGameInfo(frame, replay.productions);

    SDL_Event e;
    while (SDL_PollEvent(&e)) {
      if (e.type == SDL_QUIT) {
        goto finish;
      }
    }

    SDL_Delay(delay_ms);
  }

finish:

  return 0;
}
