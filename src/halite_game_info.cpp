#include "halite_game_info.hpp"

HaliteGameInfo::HaliteGameInfo(int num_players) {
  territory_data.resize(num_players);
  production_data.resize(num_players);
  strength_data.resize(num_players);

  // zero the vectors
  std::fill(territory_data.begin(), territory_data.end(), 0);
  std::fill(production_data.begin(), production_data.end(), 0);
  std::fill(strength_data.begin(), strength_data.end(), 0);
}
