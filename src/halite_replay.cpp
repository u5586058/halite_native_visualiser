#include "halite_replay.hpp"

static json11::Json Get(const json11::Json &json, size_t index) {
  json11::Json r = json[index];
  if (r.is_null()) {
    throw new std::runtime_error("Json index was wrong!");
  }

  return r;
}

static json11::Json Get(const json11::Json &json, const std::string &str) {
  json11::Json r = json[str];
  if (r.is_null()) {
    throw new std::runtime_error("Json element string was wrong!");
  }

  return r;
}

static int GetInt(const json11::Json &json) {
  if (!json.is_number()) {
    throw new std::runtime_error(
        "GetInt called on something which wasn't a number!");
  }

  return json.int_value();
}

Site::Site_t(json11::Json site_json) {
  json11::Json owner_json = Get(site_json, 0);
  json11::Json strength_json = Get(site_json, 1);

  this->owner = GetInt(owner_json);
  this->strength = GetInt(strength_json);
}

HaliteReplay::HaliteReplay(const json11::Json &replay_json) {
  int version = GetInt(Get(replay_json, "version"));
  if (version != 11) {
    throw std::runtime_error(std::string("Replay is the wrong version: ") +
                             std::to_string(version));
  }

  num_players = GetInt(Get(replay_json, "num_players"));
  json11::Json productions_json = Get(replay_json, "productions");
  json11::Json frames_json = Get(replay_json, "frames");

  board_dimensions.w = GetInt(Get(replay_json, "width"));
  board_dimensions.h = GetInt(Get(replay_json, "height"));

  num_frames = GetInt(Get(replay_json, "num_frames"));

  for (const auto &frame_json : frames_json.array_items()) {
    Frame frame_vector;
    for (const auto &row_json : frame_json.array_items()) {
      std::vector<Site> row_vector;
      for (const auto &site_json : row_json.array_items()) {
        row_vector.push_back(Site(site_json));
      }
      frame_vector.push_back(row_vector);
    }

    frames.push_back(frame_vector);
  }

  for (const auto &row_json : productions_json.array_items()) {
    std::vector<uint8_t> row_vector;
    for (const auto &production_json : row_json.array_items()) {
      row_vector.push_back(GetInt(production_json));
    }
    productions.push_back(row_vector);
  }
}
