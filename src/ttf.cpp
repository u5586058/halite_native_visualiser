#include "ttf.hpp"

TTF::TTF(sdl::SDL &sdl) : sdl(sdl) {
  this->num_players = sdl.num_players;

  if (TTF_Init()) {
    throw std::runtime_error("TTF failed to initialise!");
  }

  font = TTF_OpenFont("DroidSansMono.ttf", 24);
  if (!font) {
    throw std::runtime_error("SDL_TTF failed to load \"FreeSans.ttf\"!");
  }
}

TTF::~TTF() {
  TTF_CloseFont(font);

  TTF_Quit();
}

void TTF::RenderGameInfo(const Frame &frame, const Productions &productions) {
  HaliteGameInfo game_info(num_players);
  std::vector<std::string> game_info_strings;

  CalculateGameInfo(game_info, frame, productions);

  MakeGameInfoStrings(game_info_strings, game_info);

  ClearGameInfo();

  DrawGameInfo(game_info_strings);
}

void TTF::CalculateGameInfo(HaliteGameInfo &game_info, const Frame &frame,
                            const Productions &productions) const {
  for (unsigned y = 0; y < frame.size(); y++) {
    std::vector<Site> row = frame[y];
    for (unsigned x = 0; x < row.size(); x++) {
      Site site = row.at(x);
      int owner = site.owner - 1; // neutral id is 0, players start from 1
      if (owner >= 0) {
        game_info.strength_data.at(owner) += site.strength;
        game_info.territory_data.at(owner)++;
        game_info.production_data.at(owner) += productions.at(y).at(x);
      }
    }
  }
}

void TTF::MakeGameInfoStrings(std::vector<std::string> &game_info_strings,
                              const HaliteGameInfo &game_info) const {
  game_info_strings.push_back(std::string("Territory:"));
  for (const auto &territory : game_info.territory_data) {
    game_info_strings.push_back(
        std::string("    " + std::to_string(territory)));
  }

  game_info_strings.push_back(std::string("Production:"));
  for (const auto &production : game_info.production_data) {
    game_info_strings.push_back(
        std::string("    " + std::to_string(production)));
  }
  game_info_strings.push_back(std::string("Strength:"));
  for (const auto &strength : game_info.strength_data) {
    game_info_strings.push_back(std::string("    " + std::to_string(strength)));
  }
}

void TTF::DrawGameInfo(const std::vector<std::string> &game_info_strings) {
  SDL_Rect text_rect = {sdl.board_display_dim.w, sdl.screen_dim.h / 2,
                        sdl.screen_dim.w - sdl.board_display_dim.w, 30};

  for (const auto &s : game_info_strings) {
    sdl::Surface text_surface(TTF_RenderText_Solid(font, s.c_str(), white));
    sdl.BlitSurface(text_surface.surface, NULL, &text_rect);

    text_rect.y += 30;
  }
}

void TTF::ClearGameInfo(void) {
  SDL_Rect game_info_rect = {sdl.board_display_dim.w, sdl.screen_dim.h / 2,
                             sdl.screen_dim.w - sdl.board_display_dim.w,
                             sdl.screen_dim.h / 2};
  sdl.FillRect(&game_info_rect, sdl.MapRGB(0, 0, 0));
}

void TTF::DrawGameTitle(const std::vector<std::string> &player_names) {
  SDL_Rect game_info_rect = {sdl.board_display_dim.w + 50, 0,
                             sdl.screen_dim.w - sdl.board_display_dim.w - 50,
                             30};

  auto draw_title_text = [this, &game_info_rect](const auto &text) mutable {
    sdl::Surface text_surface(TTF_RenderText_Solid(font, text.c_str(), white));
    sdl.BlitSurface(text_surface.surface, NULL, &game_info_rect);
    game_info_rect.y += 30;
  };

  std::for_each(player_names.begin(), player_names.end(), draw_title_text);

  game_info_rect.x -= 50;
  draw_title_text(std::string("https://halite.io to play!"));
  draw_title_text(std::string("http://bit.ly/2gixxGr for this visualiser"));
}
