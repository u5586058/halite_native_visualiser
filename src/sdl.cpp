#include "sdl.hpp"

using namespace sdl;

Surface::Surface(SDL_Surface *surface) {
  if (!surface) {
    throw std::runtime_error("Surface was null!");
  }

  this->surface = surface;
}

Window::Window(SDL_Window *window) {
  if (!window) {
    throw std::runtime_error("Window was null!");
  }

  this->window = window;
}

SDL::SDL(int num_players) {
  srand(time(NULL));
  this->num_players = num_players;

  SDL_DisplayMode display_mode;
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    throw std::runtime_error(
        std::string("SDL failed to initialise! SDL_Error: ") +
        std::string(SDL_GetError()));
  }

  if (SDL_GetCurrentDisplayMode(0, &display_mode)) {
    throw std::runtime_error(
        std::string("SDL failed to get display mode! SDL_Error: ") +
        std::string(SDL_GetError()));
  }

  screen_dim.w = display_mode.w;
  screen_dim.h = display_mode.h;

  board_display_dim.w = MIN(screen_dim.w, screen_dim.h);
  board_display_dim.h = screen_dim.h;

  window = std::make_unique<Window>(
      SDL_CreateWindow("Halite Replay Visualiser", SDL_WINDOWPOS_UNDEFINED,
                       SDL_WINDOWPOS_UNDEFINED, screen_dim.w, screen_dim.h,
                       SDL_WINDOW_SHOWN | SDL_WINDOW_FULLSCREEN));

  screen_surface =
      std::make_unique<Surface>(SDL_GetWindowSurface(window->window));

  // choose player colors
  player_color_choices.push_back(red);
  player_color_choices.push_back(green);
  player_color_choices.push_back(blue);
  player_color_choices.push_back(yellow);
  player_color_choices.push_back(aqua);
  player_color_choices.push_back(fuchsia);

  SDL_Rect pcolor_rect = {board_display_dim.w + 2, 2, 26, 26};

  player_colors.push_back(white); // neutral player is white

  while (num_players-- > 0) {
    const int rand_color_index = std::rand() % player_color_choices.size();
    const Color color = player_color_choices[rand_color_index];
    player_colors.push_back(color);

    FillRect(&pcolor_rect, MapRGB(color));
    pcolor_rect.y += 30;

    player_color_choices.erase(player_color_choices.begin() + rand_color_index);
  }
}

SDL::~SDL() {
  // we _must_ release the surface before quitting sdl
  // otherwise the destructor for screen_surface will be
  // called after sdl has shut down
  screen_surface.reset();
  SDL_QuitSubSystem(SDL_INIT_VIDEO);
}

void SDL::SetBoardDim(Dimensions board_dim) { this->board_dim = board_dim; }

void SDL::SetTileDim(Dimensions tile_dim) { this->tile_dim = tile_dim; }

uint32_t SDL::MapRGB(uint8_t r, uint8_t g, uint8_t b) const {
  return SDL_MapRGB(screen_surface->surface->format, r, g, b);
}

uint32_t SDL::MapRGB(Color color, uint8_t intensity) const {
  return MapRGB((color.r / 255) * intensity, (color.g / 255) * intensity,
                (color.b / 255) * intensity);
}

uint32_t SDL::MapRGB(Color color) const { return MapRGB(color, 0xff); }

void SDL::FillRect(const SDL_Rect *rect, uint32_t color) {
  if (SDL_FillRect(screen_surface->surface, rect, color) < 0) {
    throw std::runtime_error(std::string("SDL_FillRect failed! SDL_Error: ") +
                             std::string(SDL_GetError()));
  }
}

void SDL::UpdateWindowSurface(void) {
  if (SDL_UpdateWindowSurface(window->window) < 0) {
    throw std::runtime_error(
        std::string("SDL_UpdateWindowSurface failed! SDL_Error: ") +
        std::string(SDL_GetError()));
  }
}

void SDL::RenderFrame(const Frame &frame, const HaliteReplay &replay) {
  // draw the board
  for (int y = 0; y < board_dim.h; y++) {
    for (int x = 0; x < board_dim.w; x++) {
      const uint8_t tp = replay.productions.at(y).at(x) * 10;
      const uint8_t owner = frame.at(y).at(x).owner;
      const uint8_t strength = frame.at(y).at(x).strength;
      const uint8_t swidth = strength * tile_dim.w / 510;
      const uint8_t sheight = strength * tile_dim.h / 510;
      const SDL_Rect r = {x * tile_dim.w, y * tile_dim.h, tile_dim.w,
                          tile_dim.h};
      const SDL_Rect ir = {x * tile_dim.w + (tile_dim.w / 2 - swidth),
                           y * tile_dim.h + (tile_dim.w / 2 - sheight),
                           swidth * 2, sheight * 2};

      uint32_t bcolor = MapRGB(player_colors.at(owner), tp);
      uint32_t fcolor = MapRGB(player_colors.at(owner));

      FillRect(&r, bcolor);
      FillRect(&ir, fcolor);
    }
  }

  // draw the player colors in the info section
  SDL_Rect game_info_color_rect = {board_display_dim.w + 2,
                                   32 + (screen_dim.h / 2), 26, 26};
  for (int i = 0; i < 3; i++) {
    for (int player = 1; player < (num_players + 1); player++) {
      FillRect(&game_info_color_rect, MapRGB(player_colors.at(player)));
      game_info_color_rect.y += 30;
    }
    game_info_color_rect.y += 30;
  }

  // Update the surface
  UpdateWindowSurface();
}

void SDL::BlitSurface(SDL_Surface *src, SDL_Rect *srcrect, SDL_Rect *dstrect) {
  if (SDL_BlitSurface(src, srcrect, screen_surface->surface, dstrect) < 0) {
    throw std::runtime_error(
        std::string("SDL_BlitSurface failed! SDL_Error: ") +
        std::string(SDL_GetError()));
  }
}
